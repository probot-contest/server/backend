const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const stylus = require('stylus');
const multer = require('multer');
const expressWs = require('express-ws');

const upload = multer({dest: "uploads/"});

const app = express();
module.exports = app;

app.locals.upload = upload;

const server = require('./bin/www');
app.locals.expressWs = expressWs(app, server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
// noinspection JSUnresolvedFunction
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/user', require('./routes/user'));
app.use('/team', require('./routes/team'));
app.use('/containers', require('./routes/containers'));
app.use('/', require('./routes/index'));

app.get('/echo', (req, res) => {
    res.render('shell');
});
app.ws('/echo', ws => {
    ws.on('message', msg => ws.send(msg));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
// noinspection JSUnusedLocalSymbols
app.use(function (err, req, res, next) {
    console.error("Got error on the next chain", err);

    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
