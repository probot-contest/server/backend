FROM node:alpine
RUN apk add --no-cache docker
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
EXPOSE 3000
ARG script="start"
ENV script="$script"
CMD npm run "$script"
