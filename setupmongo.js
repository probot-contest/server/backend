const fs = require("fs");
const crypto = require("crypto");
const readline = require("readline");

const privateKey = fs.readFileSync('sample-key.pem', 'utf8');
const publicKey = fs.readFileSync('sample-key.pub.pem', 'utf8');
const secret = fs.readFileSync('sample-secret');
const signature = fs.readFileSync('sample-signature');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: '> ',
});
(async () => {
    const docker = require('./docker');
    const container = await docker.getContainer(process.env.MONGO_CONTAINER || 'probot_mongo_1');
    const db = await require('./db');
    try {
        rl.write(`Login using the following signature:\n\n${signature.toString('base64')}\n\n`);
        while (true) {
            const type = await new Promise(resolve => rl.question('team or user? ', resolve));
            let collection;
            switch (type) {
                case 't':
                case 'team':
                    collection = db.collection('teams');
                    break;

                case 'u':
                case 'user':
                    collection = db.collection('users');
                    break;

                default:
                    process.exit();
            }
            const name = await new Promise(resolve => rl.question('handle? ', resolve));
            await collection.insertOne({
                _id: name,
                key: publicKey,
                secret: secret,
            });
        }
    } catch (e) {
        console.error(e);
    }
})();

