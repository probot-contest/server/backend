#!/usr/bin/env node
const dbp = require('../db');
const docker = require('../docker');
const byline = require('byline');

(async () => {
    try{
        const db = await dbp;
        // noinspection JSUnresolvedFunction
        const deploymentLogCollection = await db.createCollection('deployment_log', {
            capped: true, size: 1000,
            max: 5,
        });
        if (await deploymentLogCollection.count() === 0) {
            await deploymentLogCollection.insertOne({});
        }
        // noinspection JSUnresolvedFunction
        const matchCollection = db.collection('matches');
        // noinspection JSUnresolvedFunction
        const deploymentCollection = db.collection('deployments');
        while (true) {
            const deploymentLogCursor = deploymentLogCollection.find({
                deploymentId: {$exists: true},
                processed: false,
            }, {tailable: true, awaitData: true});
            for (let deploymentEvent = await deploymentLogCursor.next(); deploymentEvent != null;
                deploymentEvent = await deploymentLogCursor.next()) {
                {
                    const {modifiedCount: logModifiedCount} = await deploymentLogCollection.updateOne(
                        {_id: deploymentEvent._id}, {$set: {processed: true}});
                    if (logModifiedCount < 1) return;
                }

                const deployment = await deploymentCollection.findOne({
                    _id: deploymentEvent.deploymentId,
                    imageId: {$exists: true}, status: {$exists: false},
                });
                if (deployment === null) return;

                console.log("\033[7mNew deployment by \033[1m%s\033[0m", deployment.team);

                //todo no awaits on updateOnes
                await deploymentCollection.updateOne({_id: deployment._id}, {$set: {status: "preparing"}});
                // noinspection JSUnresolvedFunction
                const otherDeploymentsCursor = db.collection('deployments').aggregate([
                    {$match: {team: {$ne: deployment.team}}},
                    {$group: {_id: '$team', deployment: {$last: "$$CURRENT"}}},
                    {$replaceRoot: {newRoot: "$deployment"}},
                    //todo check for no matches (yet)
                ]);
                await deploymentCollection.updateOne({_id: deployment._id}, {
                    $set: {status: {done: 0}} //fixme gather and store count too
                });
                for (let otherDeployment = await otherDeploymentsCursor.next(); otherDeployment !== null;
                    otherDeployment = await otherDeploymentsCursor.next()) {
                    console.log("\033[33mDelivering new match against \033[1m%s\033[0m",
                        otherDeployment.team);
                    const {insertedId: matchId} = await matchCollection.insertOne({
                        date: new Date(),
                        teams: [
                            {
                                team: deployment.team,
                                imageId: deployment.imageId,
                            },
                            {
                                team: otherDeployment.team,
                                imageId: otherDeployment.imageId,
                            }
                        ],
                        status: "preparing",
                    });
                    await new Promise(resolve => setTimeout(resolve, 500));
                    const container = await docker.createContainer({
                        Tty: false,
                        //AttachStdin: true,
                        //AttachStdout: true,
                        //AttachStderr: true,
                        OpenStdin: true,
                        //StdinOnce: true,
                        Env: [
                            "MATCHID=" + matchId,
                        ],
                        Image: "probot/deliver-match",
                        HostConfig: {
                            Privileged: true,
                            Binds: [
                                '/var/run/docker.sock:/var/run/docker.sock',
                            ],
                        },
                    });
                    console.log("\033[32mContainer: \033[1m%s\033[0m", container.id);
                    matchCollection.updateOne({_id: matchId}, {$set: {containerId: container.id}});
                    const stream = await container.attach({
                        stream: true,
                        stdin: true,
                        stdout: true,
                        stderr: true,
                    });
                    const outputLineStream = new byline.LineStream({keepEmptyLines: true});
                    container.modem.demuxStream(stream, outputLineStream, process.stderr);
                    {
                        let i = 0;
                        outputLineStream.on('data', async line => {
                            const points = parseFloat(line);
                            console.log("\033[32mPoints for team #%d: \033[1m%d\033[0m", i, points);
                            await matchCollection.updateOne({_id: matchId}, {
                                $set: {[`teams.${i++}.points`]: points}
                            });
                        });
                    }
                    await container.start();
                    stream.write(deployment.imageId + '\n');
                    stream.write(otherDeployment.imageId + '\n');
                    await matchCollection.updateOne({_id: matchId}, {$set: {status: "in progress"}});
                    const {StatusCode} = await container.wait();
                    await matchCollection.updateOne({_id: matchId}, {
                        $set: {status: StatusCode === 0 ? "done" : "failed", statusCode: StatusCode
                        }
                    });if (StatusCode !== 0) {
                        console.log("\033[31mImage exited with status %d\033[0m", StatusCode);
                    }
                    console.log("\033[34mDone delivering match\033[0m");
                    await deploymentCollection.updateOne({_id: deployment._id}, {$inc: {"status.done": 1}});}
                await deploymentCollection.updateOne({_id: deployment._id},
                    {$set: {status: "done"}});
            }
            console.warn(new Error("deploymentLogCursor ended"));
        }
    }catch(e){
        console.error(e);
        process.exit(1);
    }
}
)();
