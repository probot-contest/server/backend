const createError = require('http-errors');
const db = require('../db');
const crypto = require("crypto");

const ERROR = Symbol('error');
const REDIRECT = Symbol('redirect');
const SWALLOW = Symbol('swallow');

function getProtector(expressKey, requestIdKey, requestSignatureKey, collection, loginPath) {
    return function (failHandlerMethod = ERROR) {
        let failHandler;
        switch (failHandlerMethod) {
            case ERROR:
                failHandler = (req, res, next, ...args) => next(createError(...args));
                break;

            case REDIRECT:
                failHandler = (req, res) => res.redirect(loginPath);
                break;

            case SWALLOW:
                failHandler = (req, res, next) => next();
                break;

            default:
                throw new Error("failHandlerMethod is illegal");
        }

        return async (req, res, next) => {
            function findValue(key) {
                let result = undefined;
                for (let object of [req.query, req.body, req.cookies, req.user]) {
                    if (object === undefined) continue;
                    if (Object.hasOwnProperty.call(object, key)) {
                        result = object[key];
                        break;
                    }
                }

                if (typeof result !== "string") {
                    failHandler(req, res, next, 403, 'Please supply a ' + key +
                        ' using either the query string, the body or a cookie');
                    return undefined;
                } else {
                    return result;
                }
            }

            try {
                const id = findValue(requestIdKey);
                if (id === undefined) return;

                const signature = findValue(requestSignatureKey);
                if (signature === undefined) return;

                // noinspection JSUnresolvedFunction
                const document = await (await db).collection(collection).findOne({_id: id});
                if (document === null) {
                    failHandler(req, res, next, 401, 'Invalid ' + requestIdKey);
                } else {
                    //todo make possible hashing request
                    //todo provide option for user to select hash algorithm
                    const verify = crypto.createVerify('SHA256');
                    const secret = document.secret.read(0, document.secret.length());
                    verify.update(secret);
                    if (!verify.verify(document.key, signature, 'base64')) {
                        failHandler(req, res, next,
                            401, 'Signature based authentication failed. ' +
                            'Please sign your secret using your private key and supply it ' +
                            'as ' + requestSignatureKey + ' in base64 encoded format');
                    } else {
                        req[expressKey] = document;
                        next();
                    }
                }
            } catch (e) {
                next(e);
            }
        }
    }
}

module.exports = {
    ERROR,
    REDIRECT,
    SWALLOW,
    user: getProtector('user', 'userId', 'userSignature', 'users', '/user/login'),
    team: getProtector('team', 'teamId', 'teamSignature', 'teams', '/team/login'),
};