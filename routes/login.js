const express = require('express');
const fs = require("fs");

module.exports = (idKey, signatureKey, title) => {
    const router = new express.Router({});

    router.get('/login', function (req, res) {
        res.render('login', {
            title: title,
            id: req.cookies[idKey], signature: req.cookies[signatureKey],
        });
    });

    router.post('/login', require('../app').locals.upload.single('signature'),
        async (req, res, next) => {
            if (req.file) {
                req.body.signature = await new Promise(resolve => {
                    fs.readFile(req.file.path, 'base64', (err, data) => {
                        if (err) {
                            return next(err)
                        } else {
                            resolve(data)
                        }
                    })
                })
            }

            res.cookie(idKey, req.body.id);
            res.cookie(signatureKey, req.body.signature);
            //todo make sure about trailing slashes
            res.redirect('.');
            res.end();
        });

    router.post('/logout', (req, res) => {
        res.clearCookie(idKey);
        res.clearCookie(signatureKey);
        res.redirect('/');
        res.end();
    });

    return router;
};