const express = require('express');
const router = express.Router({});

// noinspection JSUnresolvedFunction
router.get('/', function (req, res) {
    res.render('index', {title: 'Express'})
});

module.exports = router;
