const express = require('express');
const protect = require("../middlewares/protect");
const db = require('../db');
const router = new express.Router({});

router.use(require('./login')('teamId', 'teamSignature', 'Team login'));

router.get('/', protect.team(protect.REDIRECT), async (req, res, next) => {
    try {
        const dbo = await db;
        // noinspection JSUnresolvedFunction
        res.render('team', {
            team: req.team,
            deployments: await dbo.collection('deployments')
                .find({team: req.team._id})
                .sort({$natural: -1})
                .toArray(),
            matches: (await dbo.collection('matches')
                .find({"teams.team": req.team._id})
                .sort({$natural: -1})
                .toArray())
                .map(match => { // transforms {teams: [..., ...]} into {teams: {me: ..., opponent: ...}}
                    //todo use projection
                    const myi = match.teams[0].team === req.team._id ? 0 : 1;
                    const oi = 1 - myi;
                    return {
                        ...match,
                        teams: {
                            me: match.teams[myi],
                            opponent: match.teams[oi],
                        },
                    }
                }),
            points: (await dbo.collection('matches').aggregate([
                {$match: {"teams.team": req.team._id}},
                {
                    $group: {
                        _id: {$map: {input: "$teams", as: "team", in: "$$team.team"}},
                        match: {$last: "$$CURRENT"},
                    }
                },
                {$replaceRoot: {newRoot: "$match"}},
                {$unwind: "$teams"},
                {$match: {"teams.team": req.team._id}},
                {$group: {_id: null, points: {$push: "$teams.points"}}},
                {$unwind: "$points"},
            ]).toArray()).map(o => o.points),
            deploymentsInProgress: await dbo.collection('deployments')
                .find({status: {$ne: "done"}})
                .toArray(),
        });
    } catch (e) {
        next(e);
    }
});

router.post('/deploy/:imageId',
    protect.team(protect.ERROR),
    protect.user(protect.SWALLOW),
    //todo rate limit
    async (req, res, next) => {
        try {
            const collection = (await db).collection('deployments');
            const document = {
                date: new Date(),
                team: req.team._id,
                imageId: req.params.imageId,
            };
            if (req.user) document.user = req.user._id;
            const {insertedId} = await collection.insertOne(document);
            res.append('X-Deployment-Id', insertedId);
            (await db).collection('deployment_log').insertOne({
                deploymentId: insertedId, processed: false,
            });
            res.redirect('..');
        } catch (e) {
            next(e);
        }
    });

module.exports = router;
