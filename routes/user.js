const express = require('express');
const protect = require("../middlewares/protect");
const docker = require('../docker');
const db = require('../db');

const router = new express.Router({});

router.use(require('./login')('userId', 'userSignature', 'User login'));

router.get('/', protect.user(protect.REDIRECT), async (req, res) => {
    try {
        const dbo = await db;
        res.render('user', {
            user: req.user,
            submissions: await dbo.collection('submissions')
                .find({user: req.user._id})
                .sort({$natural: -1})
                //todo limit
                .toArray(),
        });
    } catch (e) {
        next(e);
    }
});

router.get('/submit', /*protect.user(), */(req, res) => res.render('submit'));
router.post('/submit',
    protect.user(),
    //todo limit twice every 10 minutes
    require('../app').locals.upload.single('archive'),
    async (req, res, next) => {
        try {
            const document = {
                user: req.user._id,
                date: new Date(),
                req: {
                    body: req.body,
                    file: req.file,
                },
                status: "preparing",
            };
            //todo use collection.update rather than collection.save
            const collection = (await db).collection('submissions');
            {
                const {insertedId} = await collection.insertOne(document);
                document._id = insertedId;
            }
            res.append('X-Submission-Id', document._id);

            const container = await docker.createContainer({
                Image: 'buildpack-deps',
                Cmd: ['sh', '-c', req.body.buildCmd || "make all"],
                WorkingDir: "/src",
                Tty: true,
                //todo Disable network?
            }, undefined);
            document.containerId = container.id;
            collection.save(document);

            if (req.file !== undefined) {
                await container.putArchive(req.file.path, {path: "/src"});
            }

            await container.start();

            //todo make sure about trailing slash
            res.redirect('.');

            document.status = "building";
            collection.save(document);

            document.buildStatus = (await container.wait()).StatusCode;
            collection.save(document);
            if (document.buildStatus > 0) {
                document.status = "build error";
                collection.save(document);
                return;
            }

            document.status = "saving";
            collection.save(document);

            {
                const {Id: imageId} = await container.commit({
                    Cmd: ['sh', '-c', req.body.runCmd || "make run"],
                });
                document.imageId = imageId;
            }
            document.status = "ready";
            collection.save(document);
        } catch (e) {
            next(e);
        }
    });

module.exports = router;