const express = require('express');
const docker = require('../docker');
const websocketStream = require('websocket-stream/stream');
const protect = require("../middlewares/protect");
const {Transform} = require("stream");
const router = express.Router({});
module.exports = router;

const rShell = (options = {}) => (req, res) => res.render('shell', options);

class AutoCr extends Transform {
    // noinspection JSMethodCanBeStatic JSUnusedGlobalSymbols
    _transform(chunk, encoding, callback) {
        try {
            if (encoding === 'buffer') {
                encoding = 'utf8';
                chunk = chunk.toString(encoding);
            }

            chunk = chunk.replace(/\r?\n/g, '\r\n');

            callback(null, chunk, encoding);
        } catch (e) {
            callback(e);
        }
    }
}

class AutoLf extends Transform {
    // noinspection JSMethodCanBeStatic JSUnusedGlobalSymbols
    _transform(chunk, encoding, callback) {
        try {
            if (encoding === 'buffer') {
                encoding = 'utf8';
                chunk = chunk.toString(encoding);
            }

            chunk = chunk.replace(/[\r\n]+/g, '\n');

            callback(null, chunk, encoding);
        } catch (e) {
            callback(e);
        }
    }
}

router.get('/:id/logs', rShell({autoBack: false}));
router.ws('/:id/logs', async (ws, req) => {
    try {
        const container = (await docker).getContainer(req.params.id);
        const info = await container.inspect();
        const dockerStream = await container.logs({
            follow: true,
            stdout: req.query.stderr === undefined,
            stderr: req.query.stdout === undefined,
            since: req.query.since || 0,
            until: req.query.until || 0,
            timestamps: req.query.timestamps !== undefined,
            tail: req.query.tail === undefined ? "all" : req.query.tail,
        });

        if (typeof dockerStream === "string") {
            ws.send(dockerStream);
            ws.close();
        } else {
            const remoteStream = websocketStream(ws);
            if (info.Config.Tty) {
                dockerStream.pipe(remoteStream);
            } else {
                const outputWrapperAutoCr = new AutoCr();
                outputWrapperAutoCr.pipe(remoteStream);
                container.modem.demuxStream(dockerStream, outputWrapperAutoCr, outputWrapperAutoCr);
            }
        }
    } catch (e) {
        console.error("Error in websocket", e);
        ws.send(e.toString());
    }
});

router.get('/:id/attach', rShell({termsize: true}));
router.ws('/:id/attach/:w/:h',
    (ws, req) => protect.user(protect.ERROR)(req, undefined, async (error = null) => {
        try {
            if (error !== null) {
                ws.send("\033[31m" + error + "\033[0m");
                return;
            }

            if (req.params.id !== req.user.containerId) {
                ws.send("\033[41mInsufficient permissions to attach: Not your container\033[0m");
                return;
            }

            const container = (await docker).getContainer(req.params.id);
            const info = await container.inspect();
            const dockerStream = await container.attach({
                logs: req.query.logs !== undefined,
                stream: true,
                stdin: true,
                stdout: req.query.stderr === undefined,
                stderr: req.query.stdout === undefined,
            });
            if (!info.State.Running || info.State.Paused) {
                await container.start({}, undefined);
            }

            const remoteStream = websocketStream(ws);
            if (info.Config.Tty) {
                container.resize(req.params);
                dockerStream.pipe(remoteStream);
                remoteStream.pipe(dockerStream);
            } else {
                const outputWrapperAutoCr = new AutoCr();
                outputWrapperAutoCr.pipe(remoteStream);
                container.modem.demuxStream(dockerStream, outputWrapperAutoCr, outputWrapperAutoCr);

                const inputWrapperAutoLf = new AutoLf();
                inputWrapperAutoLf.pipe(dockerStream);
                remoteStream.pipe(inputWrapperAutoLf);
            }
        } catch (e) {
            console.error("Error in websocket", e);
            ws.send(e.toString());
        }
    }));