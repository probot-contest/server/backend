#include <cstdio>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
    int g = 1021;
    int input1 = 0, input2 = 0;
    FILE *player1_stdin  = fopen("player1_stdin" , "w");
    FILE *player2_stdin  = fopen("player2_stdin" , "w");
    FILE *player1_stdout = fopen("player1_stdout", "r");
    FILE *player2_stdout = fopen("player2_stdout", "r");
    FILE *player1_stderr = fopen("player1_stderr", "r");
    FILE *player2_stderr = fopen("player2_stderr", "r");
    fprintf(player1_stdin, "%d", g);
    fprintf(player2_stdin, "%d", g);
    fscanf(player1_stdout, "%d", &input1);
    fscanf(player2_stdout, "%d", &input2);
    g *= 2;
    if (abs(input1 - g) < abs(input2-g)) {
        printf("1\n0\n");
    } else if (abs(input1 - g) == abs(input2-g)) {
        printf("1\n1\n");
    } else {
        printf("0\n1\n");
    }
}
