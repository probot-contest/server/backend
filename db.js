const {MongoClient} = require('mongodb');
const mongoUrl = process.env.MONGO_URL || "mongodb://localhost:27017/";
async function getMongo() {
    //todo wait until server is truly available
    await new Promise(resolve => setTimeout(resolve, 1000));
    return MongoClient.connect(mongoUrl);
}
const mongo = getMongo();
const db = mongo.then(mongo => mongo.db(process.env.MONGO_DB || "probot"));
module.exports = db;
